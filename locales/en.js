export default {
  title: 'The Open GLAM Survey',
  basicStats: {
    title: 'Basic statistics',
    institutions: 'Institutions with open policies',
    countries: 'Host countries of institutions',
    volume: 'Items in collections',
    whatAbout: 'What about the other {count} countries?',
  },
  actions: {
    take: 'Take action',
    addToMap: 'Add your institution to the map',
    download: 'Download the data',
    resources: 'Additional resources',
  },
  footer: {
    join: 'Join our community',
    subscribe: 'Subscribe to our updates',
    supportedBy: 'Supported by',
    followUs: 'Follow us on',
  }
}