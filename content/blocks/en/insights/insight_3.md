---
order: 3
---

# Rights policies and terms of use

To help people understand how to reuse online collections, GLAM websites’ terms of use should be as simple and accessible as possible. In reality, though, the clarity of online open access policies varies dramatically. Terms of use and copyright statements are often generic, conflicting and lacking in detail. Sometimes they don’t expressly disclaim copyright. They’re often inconsistent across web platforms. This can impact whether and how GLAM open data is recognised and understood.

