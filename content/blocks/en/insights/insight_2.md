---
order: 2
---

# Public domain compliance

Whether _new_ rights arise from the digitisation of public domain material is a legal question that varies from one country to another. The prevailing interpretation is that no new rights arise in the non-original media resulting from the reproduction of public domain works, like digital surrogates, basic data or metadata. However, in practice, many GLAMs do claim rights in this media, even while releasing it under open licences, like CC BY and CC BY-SA.
