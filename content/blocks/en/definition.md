---
---
## What is Open GLAM?

The survey’s working definition of open access mostly follows Open Knowledge International’s Open Definition and its conformant legal tools. In other words, this means commercial use of data must be allowed. 



It also considers data published under ‘no known copyright’ and similar statements to be open access. Institutions using restrictive non-commercial and/or no derivatives licences are ineligible for inclusion in the survey.
