---
---

## Some continents take up more space than others

If we look at the number of institutions per country, we find that the relative sizes of each of the countries is out of proportion to their populations. This has many reasons. Can you think of some? 