---
---

## The Open GLAM spectrum

Individual GLAM contributions can range from a single low-resolution digital surrogate with no metadata to millions of high-resolution digital surrogates with rich metadata. Looking closer at this data always reveals more detail. For example, in some cases, that single eligible work may comprise the entire collection for the GLAM, while a GLAM with millions of digital surrogates may include everything digitized so far, yet still only a fraction of the total collection. 
