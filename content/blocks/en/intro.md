---
---

How many cultural heritage institutions today make their digital collections openly available? How do they do this and where is open access most common? Where are the gaps?

This resource presents insights and visualisations about Open GLAM, the global movement advocating open access to cultural heritage. It is based on survey data that has been gathered by Douglas McCarthy, Dr Andrea Wallace and the Open GLAM community since 2018.
