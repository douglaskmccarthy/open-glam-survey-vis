// import ol from "ol"
import { Fill, Stroke, Style } from "ol/style";


export default ({ app }, inject) => {
  inject(
    "olStyle",
    Style
  )
}
