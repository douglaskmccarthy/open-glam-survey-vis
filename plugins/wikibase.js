import WBK from "wikibase-sdk"

export default ({ app }, inject) => {
  inject(
    "wbk",
    WBK({
      instance: "https://www.wikidata.org",
      sparqlEndpoint: "https://query.wikidata.org/sparql"
    })
  )
}
