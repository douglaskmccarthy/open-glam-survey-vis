import Vue from 'vue'
import VueLayers from 'vuelayers'
// import 'vuelayers/lib/style.css' // needs css-loader
import 'vuelayers/dist/vuelayers.min.css' // for @next branch of vuelayers

Vue.use(VueLayers, {
  dataProjection: 'EPSG:4326',
})