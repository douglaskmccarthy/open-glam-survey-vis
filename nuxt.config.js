export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Open GLAM Survey',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      {
        rel: "stylesheet",
        // import Google Fonts 
        // TODO probably should change this to avoid tracking by Google 
        href: "https://fonts.googleapis.com/css2?family=Inter:wght@400;700&display=swap"
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'tachyons/css/tachyons.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/wikibase.js',
    // '~/plugins/openlayers.js',
    {
      src: '@/plugins/vuelayers.js',
      ssr: false
    },
    // {
    //   src: '@/plugins/scrollama.js'
    // }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://content.nuxtjs.org/
    '@nuxt/content',
    // allow importing scss vars in components 
    '@nuxtjs/style-resources',
    // https://i18n.nuxtjs.org/
    'nuxt-i18n',
    // https://vuelayers.github.io/
    '~/modules/vuelayers',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // internationalizatin configuration
  i18n: {
    locales: [
      { code: 'en', name:'English', iso: 'en-US', file: 'en.js', dir: 'ltr' },
      { code: 'ar', name:'العربيّة', iso: 'ar-PS', file: 'ar.js', dir: 'rtl' },
      { code: 'es', name:'Español', iso: 'en-AR', file: 'es.js', dir: 'ltr' },
    ],
    lazy: true,
    langDir: './locales/',
    defaultLocale: 'en',
    //baseUrl: 'en',
    vueI18n: {
      fallbackLocale: 'en',
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    corejs: 3,
    // babel: {
    //   presets: [
    //     '@nuxt/babel-preset-app',
    //     // 'transform-es2015-modules-commonjs'
    //     '@babel/preset-env'
    //   ]
    // }
    // transpile: ['ol', ]
  },

  styleResources: {
    scss: [
        '~/assets/css/style.scss',
    ]
  }
}
